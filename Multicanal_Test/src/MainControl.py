'''
Main steering package for pulsing the AWG with a semi Gaussin pulse, changing amplitude

Created on 3 de mai de 2016
@author: leite
'''
import ROOT
import AwgControl
import numpy as np
import time
#import matplotlib.pyplot as plt

def sGauss(size=1000) :
    """
        Create a semi-Gaussian pulse to download to AWG
    """
    dt = 100./size
    tau1 = 1e-1
    tau2 = 1e-2
    Ci   = 1e-1
    t = np.arange(0,size) * 5./size
    v = ( tau1/(Ci*(tau1 - tau2) ) ) * ( np.exp(-(t-dt)/tau1) - np.exp(-(t-dt)/tau2) )
    v = np.clip(v,0,1e9)
    #plt.plot(t,v); plt.show()
    return (t,v)

def expo(size=1000):
    """
        Create an exponential pulse
    """
    dt = 100./size
    tau1 = 1e-1
    tau2 = 1e-2
    Ci   = 1e-1
    t = np.arange(0,size) * 5./size
    v = np.exp(-(t-dt)/tau1) 
    v = np.clip(v,0,1e9)
    #plt.plot(t,v); plt.show()
    return (t,v)

#IP Address of Tek AWG
awgIP = "143.107.130.87"

#File name (must be on the AWG)
pulseFile = "4CH_PULSE.awg"

awg = AwgControl.AwgControl(awgIP)
#awg.loadWaveform(pulseFile)

#Transfer array of data to AWG
#bigger than this I have a timeout - need to increase the timeout
t,v = sGauss(5000)
t,v = expo(8000)


myWfm = "SemiGauss"
wfmSize = len(v)
awg.transferWaveform(wfmName=myWfm, wfmSize=wfmSize, format="REAL", data=v)

#Use only in case of external trigger
#awg.setAwgCustomTrigger()

channel = 1
amplitudeList = np.arange(1,40) * 0.1 

awg.setWaveform(wfmName=myWfm, channel=1)
awg.setChannelState(channel=channel,state=True)
#awg.setExternalTrigger()

#awg.setDelayPoints(channel=1,delay=1500)
run = False
while (True) :
    #for amplitude in amplitudeList:
        amplitude = raw_input("Enter amplitude :")
        amplitude = float(amplitude)
        print "Amplitude: ", amplitude
        #awg.setAwgRunState(state=False)
        
        awg.setChannelAmplitude(channel=channel,amplitude=amplitude,offset=0.0)
        #awg.setChannelState(channel=channel,state=True)
        awg.setAwgRunState(state=True)
        raw_input ("Press any key to stop")
        awg.setAwgRunState(state=False)
        #if not run :
        #    awg.setAwgRunState(state=True)
        #    run = True
        #time.sleep(1)
        #generates 100 triggers
        #print "----->  Starting 1000 triggers "
        #for tr in range (100000) : awg.forceTrigger()
        #print "----->  Stop triggering "
        time.sleep(1)
        
        
        
awg.setAwgRunState(state=False)