'''
Created on Sep 3, 2013

@author: adctest
'''

import string
import struct
import time
import vxi11
import numpy as np
import sys

class AwgControl():
    '''
    Provides the control for the Tektronix AWG5014C via Ethernet (vxi11)
    The vxi module used is from
    http://alexforencich.com/wiki/en/python-vxi11/start
    '''

    def initMessages(self):
        self.INFO =    "INFO: ===> "
        self.ERROR =   "ERROR: ===> "
        self.WARNING = "WARNING: ===> "

    def __init__(self,awgIp):
        '''
            Mainly initialization
        '''

        self.awgIp = awgIp
        self.initMessages()


        print ("\n")
        print ("================================================================")
        print ("---> INFO: Connecting to Tektronix AWG5014 at ", self.awgIp)
        print ("================================================================\n")

        self.awg = vxi11.Instrument(self.awgIp)
        ret = self.awg.ask("*IDN?")

        print("--->INFO: ", ret)
        print("--->INFO: Initializing the instrument ")

        self.awg.clear()
        self.awg.write("*RST")
        self.awg.write("*CLS")

        self.setAwgRunState(state=False)
        self.setChannelState(channel=1,state=False)
        self.setChannelState(channel=2,state=False)
        self.setChannelState(channel=3,state=False)
        self.setChannelState(channel=4,state=False)

    def forceTrigger(self):
        #send a trigger to AWG now
        cmd = "Trigger:Sequence:Immediate"
        self.awg.write(cmd)

    def setExternalTrigger(self):
        #Prepares the AWG trigger to work with the Xilinx DAQ
        print (self.INFO,"Preparing AWG to Custom Trigger ")
        cmd = "Trigger:Sequence:Slope Negative"
        self.awg.write(cmd)
        cmd = "Trigger:Sequence:Source External"
        self.awg.write(cmd)
        cmd = "Trigger:Sequence:Impedance 50"
        self.awg.write(cmd)
        cmd = "AWGCONTROL:RMODE TRIGGERED"
        self.awg.write(cmd)

    def setExternalOsc(self):
        #External clock reference
        print (self.INFO,"Preparing AWG to External Clock Reference")
        cmd = "SOURCE1:ROSCILLATOR:SOURCE EXTERNAL"
        self.awg.write(cmd)
        cmd = "SOURCE1:ROSCILLATOR:FREQ 10MHZ"
        self.awg.write(cmd)
        time.sleep(1)


    def loadWaveform(self,waveformName = None):
        print (self.INFO,"Loading Waveform ", waveformName)
        wfmDir = "C:\\USERS\\OEM\\Desktop\\WAVEFORMS\\"
        wfm = '"' + wfmDir+waveformName + '"'
        cmd = string.join(['awgcontrol:srestore ', wfm])
        self.awg.write(cmd)
        time.sleep(1)

    def setChannelAmplitude (self,channel=1,amplitude=0.1,offset=0.0):

        print (self.INFO, "Setting Channel ", channel, " Amplitude=",str(amplitude)," Offset=",str(offset))

        cmd = string.join(["source",str(channel),":voltage:amplitude ",str(amplitude)],"")
        self.awg.write(cmd)
        #raw_input("...")
        cmd = string.join(["source",str(channel),":voltage:offset ",str(offset)],"")
        self.awg.write(cmd)

        time.sleep(1)

    def setAwgRunState (self,state=False):

        if state : st = "RUN"
        else : st = "STOP"
        print (self.INFO, "Setting AWG Running State to ", state)

        cmd = string.join(["AWGControl:",st],"")
        self.awg.write(cmd)
        time.sleep(1)

    def setChannelState (self,channel=1, state=False):

        if state : st = " ON"
        else : st = " OFF"
        print (self.INFO, "Setting AWG Channel ", channel,  st)

        cmd = string.join(["output",str(channel),st],"")
        self.awg.write(cmd)
        time.sleep(1)

    def transferWaveform(self,wfmName="TestWave", wfmSize=1024, format="REAL", data=None) :
        #Create a waveform in the AWG
        print (self.INFO, "Creating waveform ", wfmName, " with ", wfmSize, " points.")
        wfmName = '"' + wfmName + '"'
        cmd = 'WLIST:WAVEFORM:NEW ' + wfmName + ', ' + str(wfmSize) + ', ' + format
        print cmd
        self.awg.write(cmd)
        time.sleep(1)

        #Send the block data to AWG
        #IEEE 488 data (for now, float only) Also needs to send the data (5th byte in each
        #element of the block data, not sure how to do this, see online help
        #reload(sys)
        #sys.setdefaultencoding("ISO-8859-1")
        print (self.INFO, "Sending waveform ", wfmName)
        sData = '#' + str(len(str(wfmSize*5))) + str(wfmSize*5)
        marker = 0x0
        for i in data : sData += struct.pack("<fB",i,marker)
        cmd = 'WLIST:WAVEFORM:DATA ' + wfmName + ', ' + '0' + ', ' + str(wfmSize) + ', ' + sData
        self.awg.write_raw(cmd)
        time.sleep(1)
        
        #Normalize the waveform in the AWG to full scale
        cmd = 'WLIST:WAVEFORM:NORMALIZE ' + wfmName + ', ' + 'ZREF' 
        self.awg.write(cmd)
        time.sleep(1)
 
    def setWaveform(self, wfmName="TestWave", channel=1) :
        #Set a waveform into a given channel
        cmd = 'SOURCE' + str(channel) + ':Waveform ' + '"' + wfmName + '"'
        print cmd
        self.awg.write(cmd)
        time.sleep(1)
        
    def setDelaySeconds(self,channel=1, delay='0NS'):
        #Set the delay in a given channel in seconds
        cmd = 'SOURCE' + str(channel) + ':DELAY:ADJUST ' + delay
        self.awg.write(cmd)
        time.sleep(1)
    
    def setDelayPoints(self,channel=1, delay=0):
        #Set the delay in a given channel in points
        cmd = 'SOURCE' + str(channel) + ':DELAY:POINTS ' + str(delay)
        print cmd
        self.awg.write(cmd)
        time.sleep(1)
        
##################################################################################
if __name__ == '__main__':
    #xx = AwgControl("192.168.0.124")
    xx = AwgControl("143.107.130.87")
    xx.loadWaveform("4CH_PULSE.awg")
    #xx.loadWaveform("4CH_SINE_500k.awg")
    #xx.setAwgCustomTrigger()

    amplitudeList = [0.00, 0.040, 0.050, 0.100, 0.150, 0.200, 0.250, 0.300, 0.350, 0.400, 0.450]
    channelList = [1,2,3,4]
    for amplitude in amplitudeList:
        xx.setAwgRunState(state=False)
        for channel in channelList :
            xx.setChannelAmplitude(channel=channel,amplitude=amplitude,offset=0.0)
            xx.setChannelState(channel=channel,state=True)
        xx.setAwgRunState(state=True)



