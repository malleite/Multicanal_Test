'''
Created on Feb 22, 2014

@author: leite
'''

if __name__ == '__main__':
    pass


import usb1
import libusb1
import time
import string
import ROOT

class multicanal() :

    def __init__(self) :

        context = usb1.USBContext()
        vendor_id=0x1cbe
        device_id=3

        self.handle = context.openByVendorIDAndProductID(vendor_id, device_id)
        self.deviceL = context.getDeviceList()

        for i in range(len(self.deviceL)) :
            if (self.deviceL[i].getVendorID == vendor_id)  & (self.deviceL[i].getProductID == device_id) :
                self.device = self.deviceL[i]

        # Prepare the configuration dictionaries
        """
        This is what the host will send
        * mc_command defines the command structure :
        * status : to be defined ...
        * DAC1: value to write into DAC1
        * DAC2: value to write into DAC2
        * mode: ADC1 only, ADC2 only, ADC1 and ADC2 independent or coincidence mode
        *       xxxxx001 : ADC 1 only
        *       xxxxx010 : ADC 2 only
        *       xxxxx011 : ADC 1 and 2, independent
        *       xxxxx111 : ADC 1 and 2 in coincidence
        * maxtime: maximum time to wait before tranferring whatever is in the buffer
        * maxevents: maximum number of events (buffer size) for each adc before transferring
        * operation: xxxxxxx0 : stop acquisition
        *            xxxxxxx1 : start acquisition
        """

        self.mcCommand = {}
        self.mcCommand["setConfig"]       = 0b00000001
        self.mcCommand["setConfigLength"] = 7

        self.mcConfig = {}
        self.mcConfig["status"] = 1
        self.mcConfig["DAC1_lsb"] = 0x4f
        self.mcConfig["DAC1_msb"] = 0x00
        self.mcConfig["DAC2_lsb"] = 0x4f
        self.mcConfig["DAC2_msb"] = 0x00
        self.mcConfig["mode"] = 1
        self.mcConfig["maxTime"] = 0
        self.mcConfig["maxEvents"] = 100
        self.mcConfig["operation"] = 1

        self.handle.claimInterface(0)
        
        self.h_ADC1 = ROOT.TH1F("ADC1","ADC1",4096,0,4096)
        self.h_ADC2 = ROOT.TH1F("ADC2","ADC2",4096,0,4096)
        self.h_ADCx = ROOT.TH2F("ADCx","ADCx",256,0,256,256,0,256)
        self.h_ADC2.SetLineColor(2)


    def sendConfiguration(self,tipo = 0):
        """
        Build a string to send to microcontroller.
        There is a commnad to notify the operation requested is a MC configuration
        followed by 7 registers
        """
        
        self.mcConfig["operation"] = tipo 
        
        c = []
        c.append(self.mcCommand["setConfig"])
        c.append(self.mcCommand["setConfigLength"])

        c.append(self.mcConfig["status"])
        c.append(self.mcConfig["DAC1_lsb"])
        c.append(self.mcConfig["DAC1_msb"])
        c.append(self.mcConfig["DAC2_lsb"])
        c.append(self.mcConfig["DAC2_msb"])
        c.append(self.mcConfig["mode"])
        c.append(self.mcConfig["maxTime"])
        c.append(self.mcConfig["maxEvents"])
        c.append(self.mcConfig["operation"])

        #Append some bytes to make size 16 bytes
        for i in range(5) : c.append(0)

        command = string.join(map(chr,c),"")
        """
        print ""
        print 80*"="
        print "[WRITE] Sending config: ",  c
        print "[WRITE] Sending command: ", str(command)
        print "[WRITE] Packet size sent: ", len(command)
        print 80*"="
        """
        #TODO ADD the checksum
        self.handle.bulkWrite(1,command)

    def getData(self,channel=1) :
        self.zz = []
        timer1 = 0
        timer2 = 0
        ADC1 = 0
        ADC = 0
        ok = False 
        try :
            self.zz= self.handle.bulkRead(1,256,100)
            ok = True
        except usb1.USBErrorTimeout:
            print "Erro timeout"
            ok = False
            pass
        if ok & (len(self.zz) >=9):
            yy = map (ord,self.zz)
            timer = (yy[1]<<24) + (yy[2]<<16) + (yy[3]<<8) + yy[4]
            print "Payload Size: ", yy[0]
            if len(yy) > 5 :
                for i in range (5,(yy[0]-5),2) :
                #for i in range (5,9,2) :
                        print "------:: ",i, hex(yy[i]), hex(yy[i+1])
                        if channel == 1 : 
                            ADC1   = (yy[i]<<8) + yy[i+1]
                            #self.h_ADCx.Fill(i,yy[i+1])
                            self.h_ADC1.Fill(ADC1)

                        if channel == 2 : 
                            ADC2   = (yy[i]<<8) + yy[i+1]
                            self.h_ADCx.Fill(i,yy[i+1])
                            self.h_ADC2.Fill(ADC2)
                """
                if (yy[0] != 5) :
                    print 80*"="
                    print "[READ] ",
                    for i in yy : print i," ",
                    print " --> Timer, ADC1: ",timer, ADC1
                    print "\n[READ] Packet Size Received: ", len(self.zz)
                    print 80*"="
                """

    def end(self) :
        self.handle.close()


x = multicanal()
kk = 0 
ii = 1
c1 = ROOT.TCanvas()
#x.h_ADC1.Draw()
x.h_ADC2.Draw()
#x.h_ADCx.Draw("box,color,z")
x.sendConfiguration(tipo=2)
while True :
    ii = kk%2
    ii = 0
    print "--------> Step : ", kk, ii
    #x.sendConfiguration(tipo=0)
    #x.getData(1)
    x.sendConfiguration(tipo=1)
    x.getData(2)
    #time.sleep(0.01)
    c1.Modified()
    c1.Update()
    kk += 1 
    
    
x.end()

